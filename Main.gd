extends Node2D

signal prepare_step
signal step

export var neighborhood: bool = true
onready var step_timer: Timer = $StepTimer 
onready var label: Label = $Hud/Label
var occupied = 0

func _ready():
    var file: File = File.new()
    file.open('res://input.txt', File.READ)
    var y = 0
    var seat_scene = preload('res://Seat.tscn')
    while not file.eof_reached():
        var line: String = file.get_line()
        if not line.empty():
            var x = 0
            for c in line.to_ascii():
                var character = PoolByteArray([c]).get_string_from_ascii()
                if character == 'L':
                    var seat: Seat = seat_scene.instance()
                    seat.shift_time = step_timer.wait_time
                    if neighborhood:
                        self.connect('prepare_step', seat, 'prepare_step_neighborhood')
                    else:
                        self.connect('prepare_step', seat, 'prepare_step_visual')
                    self.connect('step', seat, 'step')
                    seat.connect('fill', self, 'occupy')
                    seat.connect('empty', self, 'leave')
                    seat.position = Vector2(x * 66, y * 66)
                    add_child(seat)
                x += 1
            y += 1
    file.close()
    step_timer.start()

func step():
    emit_signal('prepare_step')
    emit_signal('step')

func occupy():
    occupied += 1
    label.text = '%d' % occupied

func leave():
    occupied -= 1
    label.text = '%d' % occupied
