class_name Seat
extends Area2D

signal fill
signal empty

onready var animation_player: AnimationPlayer = $AnimationPlayer
onready var visual_detectors_container: Node2D = $VisualDetectors
onready var neighbor_detector: Area2D = $NeighborDetector

var visual_detectors: Array = []
var occupied = false
var next_occupied = false
var shift_time: float = 1.0

func _ready():
    animation_player.playback_speed = 1.0 / shift_time
    for child in visual_detectors_container.get_children():
        if child is RayCast2D:
            visual_detectors.push_back(child)

func prepare_step_neighborhood():
    var near = neighbor_detector.get_overlapping_areas().size()
    if occupied:
        # including self
        if near > 4:
            next_occupied = false
    else:
        if near == 0:
            next_occupied = true


func prepare_step_visible():
    var seen = 0
    for visual_detector in visual_detectors:
        if visual_detector.get_collider():
            seen += 1

    if occupied:
        # excluding self
        if seen > 4:
            next_occupied = false
    else:
        if seen == 0:
            next_occupied = true

func step():
    if occupied != next_occupied:
        if occupied:
            animation_player.play('empty')
            collision_layer = 0
            emit_signal('empty')
        else:
            collision_layer = 1
            animation_player.play('fill')
            emit_signal('fill')
        occupied = next_occupied
